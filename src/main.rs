pub mod dsp;
pub mod conversion;
pub mod music_theory;
pub mod nodes;
pub mod tui;
pub mod sound_io;

use music_theory::{Arpeggiator, DiatonicChord, DiatonicMode, DiatonicScale};
use sound_io::TestToneState;

use std::error::Error;

pub type Result<T> = std::result::Result<T, Box<dyn Error>>;

// IO config:
const OUT_FILE: &str = "arpeggio.wav";
const CHANNEL_COUNT: u16 = 2;
const SAMPLE_RATE: u32 = 48000;
const BUFFER_SIZE: u32 = 256;
const MASTER_GAIN: f32 = 0.1;

// music config:
const BPM: f32 = 480.0;
const BEATS_PER_CHORD: u64 = 16;
const CHORDS: [(u16, u16); 4] = [
    (1, 4),
    (4, 4),
    (3, 4),
    (2, 4),
];
const ARP_PATTERN: [usize; 4] = [0, 1, 2, 3];
const H: f32 = 1.0;
const M: f32 = 0.8;
const L: f32 = 0.6;
const GAIN_PATTERN: [f32; 8] = [H, L, M, H, L, M, H, L];
const SCALE_ROOT: u16 = 35;
const SCALE_MODE: DiatonicMode = DiatonicMode::Phrygian;
const DELAY_SECONDS: f32 = 0.05;
const DELAY_BALANCE: f32 = 0.3;
const DELAY_FEEDBACK: f32 = 0.02;
const DELAY_CUTOFF: f32 = 0.0001;

// constants derived from constants:
const BEATS_TO_RENDER: usize = 2 * CHORDS.len() * BEATS_PER_CHORD as usize + 1;
const BPS: f32 = BPM / 60.0;
const SAMPLES_PER_BEAT: u64 = CHANNEL_COUNT as u64 * (SAMPLE_RATE as f32 / BPS) as u64;
const SAMPLES_TO_RENDER: usize = BEATS_TO_RENDER * SAMPLES_PER_BEAT as usize;

pub struct AudioSessionState {
    pub sample_clock: sound_io::Clock,
    pub arp: Arpeggiator,
    pub osc_1: TestToneState,
    pub osc_2: TestToneState,
}

pub fn new_delay_impulse(
    time_samples: usize,
    dry_gain: f32, 
    delay_gain: f32,
    feedback: f32,
    min_gain: f32,
) -> Vec<f32> {
    if time_samples < 2 {
        return vec![1.0];
    }

    let mut impulse = vec![dry_gain];

    let mut gain = delay_gain;
    while gain > min_gain {
        impulse.extend(vec![0.0; time_samples]);

        let i_delay = impulse.len()-1;
        impulse[i_delay] = delay_gain;

        gain *= feedback;
    }

    impulse
}

pub fn next_sample(
    state: &mut AudioSessionState,
) -> f32 {
    let beat_clock = state.sample_clock.get_child("beat").unwrap();
    let beat = beat_clock.value;

    if beat_clock.just_ticked() {
        let note = state.arp.tick(beat);
        let gain_i = beat as usize % GAIN_PATTERN.len();
        let relative_gain = GAIN_PATTERN[gain_i];

        state.osc_1.frequency = conversion::midinote_to_freq(note as u8);
        state.osc_2.frequency = conversion::midinote_to_freq(note as u8 + 24);

        state.osc_1.gain = relative_gain;
        state.osc_2.gain = state.osc_1.gain;
    }

    let sample = 
        state.osc_1.get_sample(state.sample_clock.value) 
        + state.osc_2.get_sample(state.sample_clock.value);

    state.sample_clock.tick();
    sample
}

fn main() {
    let config = sound_io::OutputConfigFilters {
        channel_count: Some(CHANNEL_COUNT),
        sample_rate: Some(SAMPLE_RATE),
        buffer_size: Some(BUFFER_SIZE),
        bit_depth: Some(cpal::SampleFormat::F32),
    };

    let mut sample_clock = sound_io::Clock::new("sample", 0, None, 1);
    sample_clock.push_child(
        sound_io::Clock::new("beat", 0, None, SAMPLES_PER_BEAT)
    ).expect("Could not create beat clock.");
    
    let arp = music_theory::Arpeggiator::new(
        DiatonicScale::new(SCALE_ROOT, SCALE_MODE).render(),
        CHORDS.iter().map(|(root, size)| {
            DiatonicChord {
                parent_scale: DiatonicScale::new(SCALE_ROOT, SCALE_MODE).render(),
                root: *root,
                size: *size,
                interval: 3,
            }
        }).collect(),
        ARP_PATTERN.into(),
        BEATS_PER_CHORD, 
        CHANNEL_COUNT,
        SAMPLE_RATE
    );

    let osc_1 = sound_io::TestToneState {
        master_gain: MASTER_GAIN,
        frequency: 0.0,
        gain: 0.0,
        signal: false,
        config: config.into(),
    };
    let mut osc_2 = osc_1.clone();
    osc_2.master_gain = MASTER_GAIN * 0.5;

    let mut state = AudioSessionState { sample_clock, arp, osc_1, osc_2, };

    let delay_len = (CHANNEL_COUNT as f32 * SAMPLE_RATE as f32 * DELAY_SECONDS) as u64;
    let delay_impulse = new_delay_impulse(
        delay_len as usize, 
        1.0, 
        DELAY_BALANCE, 
        DELAY_FEEDBACK, 
        DELAY_CUTOFF
    );

    let mut samples = (0..SAMPLES_TO_RENDER).map(|_| { next_sample(&mut state)}).collect::<Vec<_>>();
    samples = dsp::convolve(&samples, &delay_impulse);

    let mut file_writer = sound_io::WavFileWriter::new(OUT_FILE.into(), config);
    file_writer.dump(&samples[..]).expect("Could not write to file.");
}
