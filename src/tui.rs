use std::error::Error;
use std::str::FromStr;
use std::io::{self, Write};

extern crate cpal;
use cpal::traits::DeviceTrait;
use crate::sound_io::*;

type Result<T> = std::result::Result<T, Box<dyn Error>>;

pub fn prompt_create_stream(process: impl FnMut(&mut [f32]) + Send + 'static) -> Result<cpal::Stream> {
    space();

    let host_options = scan_hosts();
    let host = prompt_host(&host_options)?;
    println!("Using host: {}", host.id().name());
    space();

    println!("Scanning for outputs...");
    divider();
    let output_options = scan_outputs(&host)?;
    divider();
    space();

    let output = prompt_output(&output_options)?;
    println!("Using output: {}", output.name()?);
    space();

    let all_configs = output.supported_output_configs().unwrap().collect();
    let config_values = prompt_output_config(&all_configs);
    space();

    println!("Using output config:");
    println!("{}", config_values.to_string());
    space();

    init_stream(output, config_values, process)
}

pub fn prompt_host(host_options: &HostOptions) -> Result<cpal::Host> {
    println!("Available audio hosts:");
    ordered_list(
        host_options.host_names.iter().map(|hn| hn.as_str()).collect(),
        host_options.default_host_name.as_str()
    );

    let host_index = prompt::<usize>("Choose a host: ");
    let host_name = host_options.host_names.get(host_index)
        .ok_or("Host index out of bounds.")?;
    let host = get_cpal_host(host_options, host_name)?;

    Ok(host)
}

pub fn prompt_output(output_options: &OutputOptions) -> Result<&cpal::Device> {
    println!("Available audio output devices:");
    ordered_list(
        output_options.output_names.iter().map(|on| on.as_str()).collect(), 
        output_options.default_output_name.as_str()
    );
    let output_index = prompt::<usize>("Choose an output: ");
    let output_name = output_options.output_names.get(output_index)
        .ok_or("Output index out of bounds.")?;
    let output = get_cpal_output(output_options, output_name)?;

    Ok(output)
}

pub fn prompt_output_config(all_configs: &Vec<cpal::SupportedStreamConfigRange>) -> OutputConfigFilters {
    let mut filters = OutputConfigFilters::default();
    filters.bit_depth = Some(cpal::SampleFormat::F32);

    println!("Output config template:");
    println!("{}", filters.to_string());
    space();

    while !filters.are_all_some() {
        if filters.channel_count.is_none() {
            let channel_count = prompt::<u16>("Enter channel count: ");
            filters.channel_count = Some(channel_count);
        } else if filters.sample_rate.is_none() {
            let sample_rate = prompt::<u32>("Enter sample rate: ");
            filters.sample_rate = Some(sample_rate);
        } else if filters.buffer_size.is_none() {
            let buffer_size = prompt::<u32>("Enter buffer size: ");
            filters.buffer_size = Some(buffer_size);
        }/* else if !filters.bit_depth.is_some() {
            let bit_depth = prompt::<BitDepth>("Enter bit depth: ");
            filters.bit_depth = Some(bit_depth);
        }*/

        let filtered_configs = filter_cpal_configs(all_configs, &filters);
        if filtered_configs.is_empty() {
            filters = OutputConfigFilters::default();
            filters.bit_depth = Some(cpal::SampleFormat::F32);
            space();

            println!(
                "No configs match the current selection. Cleared input:\n{}",
                filters.to_string()
            );
        }
    }

    filters
}

pub fn prompt<T : FromStr>(message: &str) -> T {
    loop {
        print!("{}", message);
        io::stdout().flush().unwrap();
        
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read input");
        let value = input.trim().parse();
        match value {
            Ok(v) => return v,
            Err(_) => {
                println!("Invalid input");
                continue;
            }
        }
    }
}

pub fn ordered_list(items: Vec<&str>, default_item: &str) {
    for (item_index, item_name) in items.iter().enumerate() {
        let is_default = default_item == *item_name;
        let suffix = { if is_default { " (default)" } else { "" } };
        println!("\t{}: {}{}", item_index, item_name, suffix);
    }
}

pub fn wait_for_enter() {
    io::stdin().read_line(&mut String::new()).unwrap();
}

pub fn space() {
    println!();
}

pub fn divider() {
    println!("--------------------------------------------------");
}
