pub type AudioBuffer = Vec<f32>;

use std::ptr;
use std::rc::Rc;
use std::cell::RefCell;

pub struct AudioNode {
	pub connections: Vec<NodeConnection>,
	pub input_ports: Vec<Option<AudioBuffer>>,
	pub output_ports: Vec<Option<AudioBuffer>>,
	pub process: fn(&mut AudioNode),
}

#[derive(Clone)]
pub struct NodeConnection {
	pub source_port: i16,
	pub destination_node: Rc<RefCell<AudioNode>>,
	pub destination_port: i16,
}

impl PartialEq for NodeConnection {
	fn eq(&self, other: &Self) -> bool {
		ptr::eq(&self.destination_node, &other.destination_node) &&
			self.source_port == other.source_port &&
			self.destination_port == other.destination_port
	}
}

impl AudioNode {
	pub fn new(process: fn(&mut AudioNode)) -> AudioNode {
		AudioNode {
			connections: Vec::new(),
			input_ports: Vec::new(),
			output_ports: Vec::new(),
			process,
		}
	}

	pub fn connect(
		&mut self,
		source_port: i16,
		destination_node: Rc<RefCell<AudioNode>>,
		destination_port: i16
	) -> NodeConnection {
		let new_connection = NodeConnection {
			source_port,
			destination_node,
			destination_port,
		};
		self.connections.push(new_connection.clone());
		new_connection
	}

	pub fn disconnect(&mut self, connection: Rc<NodeConnection>) -> Option<NodeConnection> {
		if self.connections.is_empty() {
			return None;
		}

		let index = self.connections.iter().position(|c| *c == *connection).unwrap();
		Some(self.connections.swap_remove(index))
	}

	pub fn process(&mut self) {
		(self.process)(self);
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_new() {
		let node = AudioNode::new(|_| {}); 
		assert_eq!(node.connections.len(), 0);
		assert_eq!(node.input_ports.len(), 0);
		assert_eq!(node.output_ports.len(), 0);
	}

	#[test]
	fn test_connect() {
		let node1_raw = AudioNode::new(|_| {});
		let node2_raw = AudioNode::new(|_| {});
		let node1 = Rc::new(RefCell::new(node1_raw));
		let node2 = Rc::new(RefCell::new(node2_raw));
		let connection = node1.borrow_mut().connect(0, Rc::clone(&node2), 0);
		assert_eq!(node1.borrow().connections.len(), 1);
		assert_eq!(node2.borrow().connections.len(), 0);
		assert_eq!(connection.source_port, 0);
		assert!(Rc::ptr_eq(&connection.destination_node, &node2));
		assert_eq!(connection.destination_port, 0);
	}
}
